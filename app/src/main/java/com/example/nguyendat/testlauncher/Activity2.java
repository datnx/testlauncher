package com.example.nguyendat.testlauncher;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;

/**
 * Created by NguyenDat on 7/21/15.
 */
public class Activity2 extends Activity {

    private Handler handler = new Handler();
    private MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("Activity2","onCreate");
        setContentView(R.layout.activity_2);
        playMusic(R.raw.toon_launcher_bgm001, 100);
    }

    protected void playMusic(final int musicId, long timeDelay) {

        //Service play nhac
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("Activity2","postDelayed");
                if (mMediaPlayer == null) {
                    mMediaPlayer = MediaPlayer.create(getApplicationContext(), musicId);
                    mMediaPlayer.setVolume(50, 50);
                    mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {

                        public boolean onError(MediaPlayer mp, int what, int
                                extra) {
                            Log.e("Activity2", "setOnErrorListener");
                            onError(mMediaPlayer, what, extra);
                            return true;
                        }
                    });
                }
                mMediaPlayer.start();
            }
        }, timeDelay);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("Activity2", "onNewIntent");
        stopMusic();
        playMusic(R.raw.toon_launcher_bgm001, 100);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("Activity2", "onDestroy");
        stopMusic();
    }

    private void stopMusic() {
        if (mMediaPlayer != null) {
            try {
                mMediaPlayer.stop();
                mMediaPlayer.release();
            } finally {
                mMediaPlayer = null;
            }
        }
    }
}
